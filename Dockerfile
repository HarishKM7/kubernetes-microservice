FROM node:latest
ARG ENV
ENV ENV=$ENV

EXPOSE 80
COPY microservice .
CMD ["node", "microservice.js"]
