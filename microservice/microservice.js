const express = require('express')
const app = express()

app.get('/', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.end(
    JSON.stringify({
      date: new Date().toString(),
      pod: process.env.POD_NAME,
      env: process.env.ENV
    })
  )
})

app.listen(80, () => console.log('The microservice is ready...'))
