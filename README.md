# Kubernetes Microservice

This project is also available as [a GitLab project](https://gitlab.com/HarishKM7/kubernetes-microservice).

This microservice is currently live at [http://34.93.28.206/](http://34.93.28.206/).

NOTE: THIS MICROSERVICE IS NO LONGER LIVE.

Open that URL in your browser to send an HTTP GET to the `/` endpoint of the microservice. It'll respond with the following:

```json
{
  "date": "Thu Jul 22 2021 06:39:18 GMT+0000 (Coordinated Universal Time)",
  "pod": "microservice-d6c7b49c-l5nbw",
  "env": "dev"
}
```

## How it Works

The `microservice` directory contains a Node.js (& Express.js) based microservice. It listens on port 80 & exposes only a `/` endpoint. Anyone hitting that endpoint gets a JSON response with the current date & time, the name of the Kubernetes pod running this microservice & the environment where it's running.

The `Dockerfile` builds & containerizes the `microservice`.

The `k8s-definitions` directory defines a Kubernetes deployment & a load balanced service to expose this microservice to the world.

Finally, the GitLab CI specification tells GitLab how to build & deploy everything:

- First, the microservice is built.
- Next, it's containerized & the container pushed to Docker Hub.
- Finally, the Kubernetes deployment & service are created on a GitLab-managed Kubernetes cluster running in GCP.
